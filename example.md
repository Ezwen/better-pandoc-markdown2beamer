---
title:  Making \LaTeX\xspace Beamer less horrible to use
subtitle: A Journey into pandoc's magic world
shorttitle: Markdown, pandoc and \LaTeX Beamer
author:
- name: Main Author
  short: MainAuthor
  institute: 1
  speaker: true
- name: Some Other Author
  short: OtherAuthor
  institute: 2
- name: And a last one
  short: LastOne
  institute: 2
institute:
- name: University of Rennes 1 (IRISA), France
  short: UR1
  id: 1
- name: Some Place
  short: Place
  id: 2
useshortauthors: true
toc: true
date: \today
section-titles: false
...


# A section

## Markdown!

Yay, markdown!

- An **item**
- More *items*
    
    1. Numbered items
    2. etc.
    
- And a last one

## Title page!

- Pandoc provides a simple template language and many templates available here: `/usr/share/pandoc-1.13.2/data/templates/`
- For these slides, I've extended the pandoc beamer template with some commands and some metadata handlings for the title page (such as "institute", etc.)
- This metadata is written in YAML in the first lines of this file

## \LaTeX!

\newcommand{\mycustomcommand}{yay!}

But with pandoc we can also use \LaTeX\xspace commands:

- Here I use math: $x \geq 9000$
- Or a custom command: \mycustomcommand

However markdown cannot be used in a \LaTeX\xspace environment such as `\center`:

\begin{center}

**this is not in bold**

\end{center}


## Columns!

- Since the `columns` environment is very important in beamer, it's sad not too be able to use markdown in in
- The solution: a pandoc filter!

    - This is the file `columnfilter.py`, to be given to pandoc with the argument `--filter columnfilter.py`
    
- Then we can use `[columns] [column=<page width percentage>] [/columns]` to define columns, while being able to use markdown inside of them.

[columns]

[column=.5]

\centering

**A column with some markdown**

[column=.5]


- An
- other
- column

[/columns]

# Another section

## Animations!

Of course, \pause we can use beamer ability to animate\ldots

\pause

\begin{forceLatex}
\only<3-4>{\ldots like this!}
\end{forceLatex}

\pause

But the `only` command is not well recognized by beamer, so I've defined a dummy environment called `forceLatex` that doesn't do anything. It makes sure that the content is not parsed as markdown.

## Pictures!

- Markdown has a syntax for pictures that works with pandoc, but it doesn't have options for width, scale, etc.
- Hence \LaTeX\xspace `includegraphics` is perfect:

\begin{center}
\includegraphics[width=.3\textwidth]{example}
\end{center}

## Pictures AND Animations!

However an issue with beamer is that images cannot be uncovered with `\pause`:

\pause

\begin{center}
\includegraphics[width=.1\textwidth]{example2}
\end{center}

Hence I managed to obtain a command `includecoveredgraphics` to replace `includegraphics` in these situations:

\pause

\begin{center}
\includecoveredgraphics[width=.1\textwidth]{example3}
\end{center}

# Conclusion

## Conclusion

\centering \huge
Have fun! \smiley
