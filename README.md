# Customized markdown2beamer with pandoc

## Description

Custom pandoc beamer template and filter script adding:

- `[columns]` syntax to declare columns
- `\includecoveredgraphics` to uncover pictures with animations
- much more metadata handling (`institute`, `shorttitle`, etc.)

## Requirements

- `pandoc` (tested with 1.16.0.2)
- `pandocfilters` python module
- `pdflatex`

## Usage

An example is available in `example.md`, which can be compiled using `compile.sh`.

A script for gedit is also available, to be used in the page *"Manage external tools..."*.

## TODO

- Provide some nice and generic syntax to use any latex environments (and even commands?), instead of some ad-hoc hack for columns/column. Something like `[myenvironment(option1,option2)] stuff [/myenviroment]`.
- Provide a way to give options to frame environments (e.g. `plain`)
